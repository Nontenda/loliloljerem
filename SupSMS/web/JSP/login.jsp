<%-- 
    Document   : login
    Created on : 4 déc. 2014, 12:20:04
    Author     : julien
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<title>CONNEXION TEST AGILE</title>
	<body> 
        <c:url value="/Login" var="login" />
	<form name="formTestConnexion" action="${login}" method="POST">
		login : <input name="userName" type="text">
		</br>
		</br>
		mot de passe : <input name="password" type="text">
		<INPUT border=0 src="Contenu/valider2.png" type=image Value=submit align="middle" >
	</form>
                <c:if test="${not empty messageErreur}">
                    ${messageErreur}
                    <c:url value="/Enregistrement" var="Enregistrement" />
                    <a href="${Enregistrement}">S'enregistrer ?</a>
                </c:if>
	</body>
</html>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Ja'chat</title>
		<meta name="description" content="Minimal Form Interface: Simplistic, single input view form" />
		<meta name="keywords" content="form, minimal, interface, single input, big form, responsive form, transition" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="container">
			<!-- Top Navigation -->
			<header class="codrops-header">
				<h1 style="color:red;">Ja'chat<span>Accueil</span></h1>	
			</header>
			<section>
				
					<div class="simform-inner">
					<c:url value="/Login" var="login" />
                                        <form name="formTestConnexion" action="${login}" method="POST">
						Login : <input name="userName" type="text">
						</br>
						</br>
						Mot de passe : <input name="password" type="text">
					
					
						<INPUT border=0 type="submit" align="middle" >
                                                <INPUT border=0 src="Contenu/valider2.png" type=image Value=submit align="middle" >
						</br>
						</br>
						Pas de compte ? S'inscrire <a href="index.html">ici</a>
						
					</form>
                                        <c:if test="${not empty messageErreur}">
                                            ${messageErreur}
                                            <c:url value="/Enregistrement" var="Enregistrement" />
                                            <a href="${Enregistrement}">S'enregistrer ?</a>
                                        </c:if>	
                                    </div><!-- /simform-inner -->
			</section>
			
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/stepsForm.js"></script>
	</body>
</html>
