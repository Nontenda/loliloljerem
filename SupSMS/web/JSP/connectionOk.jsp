<%-- 
    Document   : connectionOk
    Created on : 4 déc. 2014, 12:23:22
    Author     : julien
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Connect&eacute;</title>
    </head>
    <body>
        <h1>Bonjour ${requestScope['nom']} ${requestScope['prenom']} tu est maintenant connecté !</h1>
        
        <p>Voici la liste de tous les utilisateurs : </p>
        <c:forEach items="${requestScope['listeUtilisateurs']}" var="i">
            Utilisateur :  <c:out value="${i.nom}"/><c:out value="  ${i.id}"/>
                <c:url value="/AjouterContact" var="AjouterContact">
                    <c:param name="UtilisateurId" value="${i.id}" />
                </c:url>
                <a href="${AjouterContact}">Ajouter aux contacts ?</a><p>
        </c:forEach>
    </body>
</html>
