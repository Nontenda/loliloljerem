<%-- 
    Document   : enregistrement
    Created on : 4 déc. 2014, 12:41:44
    Author     : julien
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Ja'chat</title>
		<meta name="description" content="Minimal Form Interface: Simplistic, single input view form" />
		<meta name="keywords" content="form, minimal, interface, single input, big form, responsive form, transition" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="WEB-INF/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="WEB-INF//css/demo.css" />
		<link rel="stylesheet" type="text/css" href="WEB-INF//css/component.css" />
		<script src="WEB-INF/js/modernizr.custom.js"></script>
	</head>
	<body>
		<div class="container">
			<!-- Top Navigation -->
			<header class="codrops-header">
				<h1 style="color:red;">Ja'chat<span>Inscription</span></h1>	
			</header>
			<section>
				<form id="theForm" class="simform" autocomplete="off">
					<div class="simform-inner">
						<ol class="questions">
							<li>
								<span><label style="color:black;" for="q1">Rentrez votre login</label></span>
								<input id="q1" name="q1" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q2">Votre véritable nom : </label></span>
								<input id="q2" name="q2" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q3">Votre ville : </label></span>
								<input id="q3" name="q3" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q4">Votre mot de passe : </label></span>
								<input type="password" id="q4" name="q4" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q5">Votre question secrète ... </label></span>
								<input id="q5" name="q5" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q5">... votre réponse :</label></span>
								<input id="q5" name="q5" type="text"/>
							</li>
						</ol><!-- /questions -->
						<button class="submit" type="submit">Send answers</button>
						<div class="controls">
							<button class="next"></button>
							<div class="progress"></div>
							<span class="number">
								<span class="number-current"></span>
								<span class="number-total"></span>
							</span>
							<span class="error-message"></span>
						</div><!-- / controls -->
					</div><!-- /simform-inner -->
					<span class="final-message"></span>
				</form><!-- /simform -->			
			</section>
			
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/stepsForm.js"></script>
		<script>
			var theForm = document.getElementById( 'theForm' );

			new stepsForm( theForm, {
				onSubmit : function( form ) {
					// hide form
					classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );

					/*
					form.submit()
					or
					AJAX request (maybe show loading indicator while we don't have an answer..)
					*/

					// let's just simulate something...
					var messageEl = theForm.querySelector( '.final-message' );
					messageEl.innerHTML = 'Enregistrement confirmé !';
					classie.addClass( messageEl, 'show' );
				}
			} );
		</script>
	</body>
</html>
