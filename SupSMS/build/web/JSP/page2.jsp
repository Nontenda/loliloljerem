<%-- 
    Document   : page2
    Created on : 3 déc. 2014, 18:46:13
    Author     : julien
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>page 2!</h1>
        <c:forEach items="${requestScope['utilisateurs']}" var="i">
            Utilisateur :  <c:out value="${i.nom}"/><c:out value="  ${i.id}"/>
                <c:url value="/Delete" var="deleteUtilisateurUrl">
                    <c:param name="UtilisateurId" value="${i.id}" />
                </c:url>
                <a href="${deleteUtilisateurUrl}">Delete</a><p>
        </c:forEach>
    </body>
</html>
