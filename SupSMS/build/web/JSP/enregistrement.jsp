<%-- 
    Document   : enregistrement
    Created on : 4 déc. 2014, 12:41:44
    Author     : julien
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Ja'chat</title>
		<meta name="description" content="Minimal Form Interface: Simplistic, single input view form" />
		<meta name="keywords" content="form, minimal, interface, single input, big form, responsive form, transition" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">

		<script src="<c:url value="/js/modernizr.custom.js"/>"></script>
                
	</head>
	<body>
		<div class="container">
			<!-- Top Navigation -->
			<header class="codrops-header">
				<h1 style="color:red;">Ja'chat<span>Inscription</span></h1>	
			</header>
			<section>
                                <c:url value="/Enregistrement" var="enregistrement" />
                                <form name="formTestConnexion" action="${login}" method="POST">
					
						<ol class="questions">
                                                        <li>
								<span><label style="color:black;" for="q2">Votre username : </label></span>
								<input id="q1" name="username" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q2">Votre véritable nom : </label></span>
								<input id="q2" name="nom" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q3">Votre prenom : </label></span>
								<input id="q3" name="prenom" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q4">Votre mot de passe : </label></span>
								<input type="password" id="q4" name="motdepasse" type="text"/>
							</li>
							<li>
								<span><label style="color:black;" for="q5">Votre question mail ... </label></span>
								<input id="q5" name="courriel" type="text"/>
							</li>
							
						</ol><!-- /questions -->
						<INPUT border=0 src="Contenu/valider2.png" type=image Value=submit align="middle" >
					
					<span class="final-message"></span>
				</form><!-- /simform -->			
			</section>
			
		</div><!-- /container -->
		
	</body>
</html>



