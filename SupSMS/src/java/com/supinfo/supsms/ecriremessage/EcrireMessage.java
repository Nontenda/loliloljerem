/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.ecriremessage;


import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author julien
 */
@Stateless
public class EcrireMessage {
        
    @Resource(mappedName = "jms/queue/TestEnvoyerRecevoirFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName="jms/queue/TestEnvoyerRecevoir")
    private Queue printerQueue;

    public void envoyerMessage(String nom, String message) {
        
        this.sendMessage(nom,message);
    }
    
    
    private void sendMessage(String nom, String message) {
        Connection connection = null;
        Session session = null;

        try {
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer producer = session.createProducer(printerQueue);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            // Create a message
            
            StringBuilder sb = new StringBuilder();
            sb.append("Message de : "+nom+" \n");
            sb.append("\n"+message);
            sb.append("\n fin du message");
            /// etc... 
            TextMessage messagee = session.createTextMessage(sb.toString());
            messagee.setStringProperty("Expediteur", nom);
            messagee.setStringProperty("Destinataire", nom);

            // Tell the producer to send the message
            producer.send(messagee);
            
            System.out.println("message sent!");
        } catch (JMSException ex) {
            
        } finally {
            try {
                // Clean up
                if (session != null) session.close();
                if (connection != null) connection.close();
            } catch (JMSException ex) {
                
            }
        }
    }

}
