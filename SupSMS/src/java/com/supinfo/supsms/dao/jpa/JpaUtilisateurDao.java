/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao.jpa;

import com.supinfo.supsms.dao.UtilisateurDao;
import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Utilisateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julien
 */
@Stateless
public class JpaUtilisateurDao implements UtilisateurDao{
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Utilisateur addUtilisateur(Utilisateur utilisateur) {
        em.persist(utilisateur);
        return utilisateur;
    }

    @Override
    public List<Utilisateur> getAllUtilisateurs() {
        return em.createQuery("SELECT u FROM Utilisateur u").getResultList(); 
    }

    @Override
    public void removeUtilisateur(Utilisateur utilisateur) {
        em.remove(utilisateur); 
    }

    @Override
    public Utilisateur findUtilisateurById(Long utilisateurId) {
        return em.find(Utilisateur.class, utilisateurId); 
    }
    
    @Override
    public Utilisateur findUtilisateurByName(String name){
        //return (Utilisateur)em.createQuery("SELECT u FROM Utilisateur u WHERE u.userName = :name").getSingleResult(); 
        Query query = em.createQuery("SELECT u FROM Utilisateur u WHERE u.userName = :name");
        query.setParameter("name", name);
        List<Utilisateur> utilisateur=query.getResultList();
        if(utilisateur!=null && utilisateur.size()>0){
        return utilisateur.get(0);
        }
        else{return null;}
    }
    
    @Override
    public Utilisateur ajouterContact(Utilisateur utilisateur,Contact contact){
        em.getTransaction().begin();
        utilisateur.addContact(contact);
        em.getTransaction().commit();
        return utilisateur;
    }
}
