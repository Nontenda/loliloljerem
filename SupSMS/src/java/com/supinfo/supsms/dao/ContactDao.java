/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao;

import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Utilisateur;
import java.util.List;

/**
 *
 * @author julien
 */
public interface ContactDao {
    
    Contact ajouterContact(Contact contact);    

    Contact findContactById(Long contactId);

}
