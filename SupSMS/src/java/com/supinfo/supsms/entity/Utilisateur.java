/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.entity;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 *
 * @author julien
 */
@Entity
public class Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(nullable=false)
    private String userName;
<<<<<<< HEAD
    
    @NotNull
    @Column(nullable=false)
    private String prenom;
    
    @NotNull
    @Column(nullable=false)
    private String nom;
=======
>>>>>>> ee46e4d55fb2e7f4f6c59463837d1120d9c526d6
    
    @NotNull
    @Column(nullable=false)
    private String email;
    
    @NotNull
    @Column(nullable=false)
    private String motDePasse;
    
    @OneToMany(cascade=CascadeType.ALL)
    private List<Contact> contacts;

    public Utilisateur(){}
    
<<<<<<< HEAD
    public Utilisateur(String nom, String prenom, String email, String motDePasse, String userName) {
        this.prenom = prenom;
        this.nom = nom;
=======
    public Utilisateur(String userName, String email, String motDePasse) {
        this.userName = userName;
>>>>>>> ee46e4d55fb2e7f4f6c59463837d1120d9c526d6
        this.email = email;
        this.motDePasse = motDePasse;
        this.userName = userName;
    }

    public String getMotDePasse() {
        return motDePasse;
    }
    
    

    public String getUserName() {
        return userName;
    }
    
    public Long getId() {
        return id;
    }

<<<<<<< HEAD
    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public List<Contact> getContacts() {
        return contacts;
=======
    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the lastName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * @return the password
     */
    public String getPassword(){
        return motDePasse;
    }
    
    /**
     * @param password the password to set
     */
    public void setPassword(String password){
        this.motDePasse = password;
>>>>>>> ee46e4d55fb2e7f4f6c59463837d1120d9c526d6
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    public void addContact(Contact contact){
        if(contacts == null){
            contacts = new ArrayList<Contact>();
        }
        contacts.add(contact);
        contact.setUtilisateur(this);
    }
    
}
