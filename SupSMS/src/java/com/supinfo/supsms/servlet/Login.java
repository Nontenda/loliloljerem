/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.servlet;

import com.supinfo.supsms.entity.Utilisateur;
import com.supinfo.supsms.service.UtilisateurService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supinfo.supsms.entity.Utilisateur;
import com.supinfo.supsms.service.UtilisateurService;
import javax.ejb.EJB;
import javax.servlet.http.HttpSession;

/**
 *
 * @author julien
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    @EJB
    UtilisateurService utilisateurService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private static final String USERNAME = "userName";
    private static final String PASSWORD = "password";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/JSP/login.jsp").forward(request, response);

    }
    
    
    @EJB
    UtilisateurService utilisateurService;

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
<<<<<<< HEAD
        
        String userName = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);
        
        Utilisateur u = utilisateurService.findUtilisateurByName(userName);
        // si bon user
        if(u!=null){
            // Si mdp ok
            if(u.getMotDePasse().equals(password)){
                request.setAttribute("nom", u.getNom());
                request.setAttribute("prenom", u.getPrenom());
                List<Utilisateur> utilisateurs = utilisateurService.getAllUtilisateurs();
                HttpSession session = request.getSession();
                session.setAttribute("UtilisateurCourant", u);
                request.setAttribute("listeUtilisateurs", utilisateurs);
                request.getRequestDispatcher("/JSP/connectionOk.jsp").forward(request, response);
            }else{
                request.setAttribute("messageErreur", "mot de passe incorrect");
                request.getRequestDispatcher("/JSP/login.jsp").forward(request, response);
            }
        }else{
            request.setAttribute("messageErreur", "Utilisateur inconnu");
            request.getRequestDispatcher("/JSP/login.jsp").forward(request, response);
        }
=======

        HttpSession session = request.getSession();

        /*String userName = request.getParameter(USERNAME);
         String password = request.getParameter(PASSWORD);

         HashMap<String, String> userNames = new HashMap<String, String>();
         userNames.put("julien","password");
         userNames.put("laurent","password");
         userNames.put("anthony","password");
         userNames.put("jeremy","password");
         if(userNames.containsKey(userName)){
         if(userNames.get(userName).equals(password)){
         request.getRequestDispatcher("/JSP/connectionOk.jsp").forward(request, response);
         }else{
         request.setAttribute("messageErreur", "mot de passe incorrect");
         request.getRequestDispatcher("/JSP/login.jsp").forward(request, response);}
         }else{
         request.setAttribute("messageErreur", "Utilisateur inconnu");
         request.getRequestDispatcher("/JSP/login.jsp").forward(request, response);}*/
        System.out.println("method called");
        String userName = request.getParameter("userName");
        String passWord = request.getParameter("passWord");

        String responseString = "";
        List<Utilisateur> allUsers = utilisateurService.getAllUtilisateurs();

        if (allUsers.size() > 0) {
            for (Utilisateur user : allUsers) {
                if (user.getUserName().equals(userName)) {
                    if (user.getPassword().equals(passWord)) {
                        responseString = "User login OK";

                        session.setAttribute("UserID", user.getId());
                    } else {
                        responseString = "Incorrect password";
                    }
                }
            }
        }
        if (responseString.isEmpty()) {
            responseString = "Unknown user";
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.append(responseString);
        out.close();
    }

    private void prepareStringResponse(HttpServletResponse response, String responseString) throws IOException {

>>>>>>> ee46e4d55fb2e7f4f6c59463837d1120d9c526d6
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
