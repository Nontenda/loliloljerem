/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.servlet;

import com.supinfo.supsms.entity.Utilisateur;
import com.supinfo.supsms.service.UtilisateurService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author julien
 */
@WebServlet(name = "Enregistrement", urlPatterns = {"/Enregistrement"})
public class Enregistrement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Enregistrement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Enregistrement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/JSP/enregistrement.jsp").forward(request, response);
    }

    @EJB
    UtilisateurService utilisateurService;

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
<<<<<<< HEAD
        String nom,prenom,mail,motDePasse,username;
        username = request.getParameter("username");
        nom = request.getParameter("nom");
        prenom = request.getParameter("prenom");
        mail = request.getParameter("courriel");
        motDePasse = request.getParameter("motdepasse");
        Utilisateur util = new Utilisateur(nom,prenom,mail,motDePasse,username);
        util = utilisateurService.AjouterUtilisateur(util);
        List<Utilisateur> utilisateurs = utilisateurService.getAllUtilisateurs();
        request.setAttribute("utilisateurs", utilisateurs);
        request.getRequestDispatcher("/JSP/page2.jsp").forward(request, response);
=======
        String userName, email, passWord;
        userName = request.getParameter("userName");
        email = request.getParameter("email");
        passWord = request.getParameter("passWord");

        String responseString = "";

        if (userName.isEmpty() || email.isEmpty() || passWord.isEmpty()) {
            responseString = "One of the fields is empty";
        } else {
            Utilisateur util = new Utilisateur(userName, email, passWord);
            util = utilisateurService.AjouterUtilisateur(util);

            if (util != null && util.getId() != null) {
                responseString = "User " + util.getUserName() + " was successfully created !";
            } else {
                responseString = "Could not create user.";
            }
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.append(responseString);
        out.close();
>>>>>>> ee46e4d55fb2e7f4f6c59463837d1120d9c526d6
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
