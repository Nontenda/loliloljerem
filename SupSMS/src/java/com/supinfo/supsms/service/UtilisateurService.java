/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.UtilisateurDao;
import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Utilisateur;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author julien
 */
@Stateless
public class UtilisateurService {
    
    @EJB
    UtilisateurDao utilisateurDao;
    
    public Utilisateur AjouterUtilisateur(Utilisateur utilisateur) {
        utilisateur = utilisateurDao.addUtilisateur(utilisateur);
        return utilisateur;
    }
    
    public List<Utilisateur> getAllUtilisateurs() {
        return utilisateurDao.getAllUtilisateurs();
    }

    public void removeUtilisateur(Long utilisateurId) {
        utilisateurDao.removeUtilisateur(findUtilisateurById(utilisateurId));
    }
    
    public void removeUtilisateur(Utilisateur utilisateur) {
        utilisateurDao.removeUtilisateur(utilisateur);
    }
    
    public Utilisateur findUtilisateurById(Long userId) {
        return utilisateurDao.findUtilisateurById(userId);
    }
    
    public Utilisateur findUtilisateurByName(String name) {
        return utilisateurDao.findUtilisateurByName(name);
    }
    
    public Utilisateur ajouterContact(Utilisateur utilisateur, Contact contact){
        return utilisateurDao.ajouterContact(utilisateur, contact);
    }
}
