/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.ContactDao;
import com.supinfo.supsms.dao.UtilisateurDao;
import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Utilisateur;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author julien
 */
@Stateless
public class ContactService {   
    
    @EJB
    ContactDao contactDao;
    
    public Contact ajoutetContact(Contact contact){
        return contactDao.ajouterContact(contact);
    }
}
